import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

var OtherModule = /** @class */ (function () {
    function OtherModule() {
    }
    OtherModule_1 = OtherModule;
    var OtherModule_1;
    OtherModule = OtherModule_1 = __decorate([
        NgModule({
            imports: [
                IonicModule
            ]
        })
    ], OtherModule);
    return OtherModule;
}());
export { OtherModule };
